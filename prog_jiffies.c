#include <stdio.h>
#include <linux/unistd.h>
#include <sys/syscall.h>

#define __NR_getjiffies		338  /* segun posicion en el kernel */
#define __NR_diffjiffies	__NR_getjiffies+1  /* el siguiente */

int main(int ac, char *av[])
{
  long jifs, result;
  int err=1;
  printf("Llamadas a las system calls %d y %d \n",__NR_getjiffies,__NR_diffjiffies);
  printf("Mide el tiempo desde el arranque en jiffies,\ndespués hace una pausa y calcula la diferencia.\n");
  if(ac!=2) 
  {
	printf("Error, se debe indicar duración de la pausa en segundos.\n");
	return -1;
  }
   
  err=syscall(__NR_getjiffies, &jifs);

  printf("retorno syscall %d = %d \n", __NR_getjiffies, err); 

  if(!err) 
	printf("valor inicial de jiffies= %ld\n",jifs);
   else 
	printf("error\n");
  
  printf("Pausa de %d segundos\n",atoi(av[1]));
  sleep(atoi(av[1]));

  err=syscall( __NR_diffjiffies, jifs, &result);
  printf("retorno syscall %d = %d \n", __NR_diffjiffies, err); 
 
  if (!err) 
    printf( "diferencia en jiffies (elapsed) = %ld\n", result );  
  else 
    printf( "error\n" ); 

  return 0;
}

