asmlinkage int sys_getjiffies( long __user *presult )
{
  long cur_jiffies = (long)get_jiffies_64();
  long result;

  if (presult) {

    result = cur_jiffies;
    return( put_user( result, presult ) );

  }

  return -EFAULT;
}


asmlinkage int sys_diffjiffies( long ujiffies, long __user *presult )
{
  long cur_jiffies = (long)get_jiffies_64();
  long result;

  if (presult) {

    result = cur_jiffies - ujiffies;
    return( put_user( result, presult ) );

  }

  return -EFAULT;
}

